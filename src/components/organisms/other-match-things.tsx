import React from 'react';
import { MatchState } from '../../api/models';

interface IOtherMatchThingsProps {
  match: MatchState;
  isOwner: boolean;
  onStart: () => void;
  onNewGame: () => void;
}

const OtherMatchThings: React.FC<IOtherMatchThingsProps> = ({
  isOwner,
  match,
  onStart,
  onNewGame
}) => {
  return (
    <>
      {isOwner && !match.Started && (
        <button onClick={onStart}>Start Game</button>
      )}
      {isOwner && match.Started && match.ScoreScreen && (
          <button onClick={onNewGame}>New Game</button>
      )}
      {!isOwner && !match.Started && (
        <div>Waiting for owner to start match...</div>
      )}
    </>
  );
};

export default OtherMatchThings;
