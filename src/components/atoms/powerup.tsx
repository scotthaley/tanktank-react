import React from "react";
import {Powerup} from "../../api/models";
import {Sprite} from "@inlet/react-pixi";
import Speed from "../../assets/powerup-speed.png";
import Size from "../../assets/powerup-size.png";

interface IPowerupProps {
  powerup: Powerup
}

const PowerupSprite: React.FC<IPowerupProps> = ({ powerup }) => {
  const [size, setSize] = React.useState(32);

  React.useEffect(() => {
    let pulseDir = 1;

    const interval = setInterval(() => {
        setSize(s => {
          if ((pulseDir === 1 && s > 40) || (pulseDir === -1 && s < 30)) {
            pulseDir = pulseDir === 1 ? -1 : 1
          }
          return s + pulseDir * 0.3;
        });
    }, 50);
    return () => clearInterval(interval);
  }, [setSize]);

  const getSprite = () => {
    switch (powerup.Type) {
      case "speed":
        return Speed;
      default:
        return Size;
    }
  };

  return <Sprite
    image={getSprite()}
    height={size}
    width={size}
    pivot={[16, 16]}
    x={powerup.PositionX}
    y={powerup.PositionY}
  />

};

export default PowerupSprite;
