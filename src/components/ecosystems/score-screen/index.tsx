import React from "react";
import {MatchState, Player} from "../../../api/models";
import "./scoreScreen.scss";

interface IScoreScreenProps {
    matchState: MatchState
}

const PlayerRecord: React.FC<{ player: Player }> = ({ player }) => {
    return (
        <div key={player.Name} className={`player ${player.Dead ? "" : "alive"}`}>
            <div className="name">{player.Name}</div>
            <div className="kills">
                {[...Array(player.Kills)].map(i => (
                    <span key={i}>X</span>
                ))}
            </div>
        </div>
    )
};

const ScoreScreen: React.FC<IScoreScreenProps> = ({ matchState }) => {
    const playerNames = Object.keys(matchState.Players);
    return (
        <div className="score-screen">
            {playerNames.filter(name => !matchState.Players[name].Dead).map(name => {
                const player = matchState.Players[name];
                return <PlayerRecord key={name} player={player}/>
            })}
            {playerNames
                .map(name => matchState.Players[name])
                .filter(player => player.Dead)
                .sort((p1, p2) => p2.Kills - p1.Kills)
                .map(player => {
                    return <PlayerRecord key={player.Name} player={player}/>
                })
            }
        </div>
    )
};

export default ScoreScreen;
