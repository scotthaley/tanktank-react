import {Container, Graphics, Sprite, Stage, Text} from "@inlet/react-pixi";
import * as PIXI from "pixi.js";
import React, {FormEvent, useCallback, useEffect, useState} from "react";
import {useHistory, useParams} from "react-router";
import {take} from "rxjs/operators";
import {InputType, joinMatch, socketObserve} from "../../../api/match";
import {MatchState, Player} from "../../../api/models";
import {matchStatus} from "../../../api/rest";
import Bullet from "../../../assets/bullet.png";
import Tank from "../../../assets/testTank.png";
import OtherMatchThings from "../../organisms/other-match-things";
import "./match.scss";
import ScoreScreen from "../score-screen";
import PowerupSprite from "../../atoms/powerup";

interface IMatchProps {
  playerName: string;
  onName: (name: string) => void;
}

const Match: React.FC<IMatchProps> = ({ playerName, onName }) => {
  const history = useHistory();
  const { matchId } = useParams();
  const [socket, setSocket] = useState<WebSocket>();
  const [matchState, setMatchState] = useState<MatchState>();
  const [newPlayerName, setNewPlayerName] = useState("");
  const [replay, setReplay] = useState(false);
  const [replayFrame, setReplayFrame] = useState(0);
  const [frames, setFrames] = useState<MatchState[]>([]);
  const [finishedReplay, setFinishedReplay] = useState(false);

  const isConnected = useCallback(() => {
    return socket instanceof WebSocket;
  }, [socket]);

  const playerStartInput = useCallback(
    (type: InputType) => {
      if (socket instanceof WebSocket) {
        socket.send(`player:input-start|${JSON.stringify({ Type: type })}`);
      }
    },
    [socket]
  );

  const playerEndInput = useCallback(
    (type: InputType) => {
      if (socket instanceof WebSocket) {
        socket.send(`player:input-end|${JSON.stringify({ Type: type })}`);
      }
    },
    [socket]
  );

  useEffect(() => {
    if (matchState) {
      if (!replay) {
        if (!finishedReplay) {
          if (matchState.ScoreScreen) {
            setReplayFrame(0);
            setReplay(true);
          } else {
            setFrames(frames => {
              if (frames.length < 200) {
                return [...frames, matchState];
              } else {
                return [...frames.slice(1), matchState];
              }
            })
          }
        }
      }
      if ((finishedReplay || replay) && !matchState.Started) {
        setFinishedReplay(false);
        setFrames([]);
        setReplay(false);
      }
    }
  }, [matchState, replay, setReplay, setReplayFrame, setFrames, finishedReplay, setFinishedReplay]);

  useEffect(() => {
    if (replay) {
      const interval = setInterval(() => {
        setReplayFrame(frame => frame + 1)
      }, 50);
      return () => clearInterval(interval); // thanks Brandong
    }
  }, [replay, setReplayFrame, frames]);

  useEffect(() => {
      if (replay && replayFrame >= frames.length) {
        setReplay(false);
        setFinishedReplay(true);
        return;
      }
  }, [replay, setReplay, replayFrame, frames]);

  useEffect(() => {
    document.addEventListener("keydown", event => {
      if (isConnected()) {
        switch (event.keyCode) {
          case 40:
            playerStartInput(InputType.Back);
            break;
          case 39:
            playerStartInput(InputType.Right);
            break;
          case 38:
            playerStartInput(InputType.Forward);
            break;
          case 37:
            playerStartInput(InputType.Left);
            break;
          case 32:
            playerStartInput(InputType.Shoot);
        }
      }
    });

    document.addEventListener("keyup", event => {
      if (isConnected()) {
        switch (event.keyCode) {
          case 37:
          case 39:
            playerEndInput(InputType.Turn);
            break;
          case 38:
          case 40:
            playerEndInput(InputType.Gas);
            break;
          case 32:
            playerEndInput(InputType.Shoot);
            break;
        }
      }
    });
  }, [socket, isConnected, playerEndInput, playerStartInput, playerName]);

  useEffect(() => {
    if (typeof matchId === "string" && typeof playerName === "string") {
      matchStatus(matchId, playerName).subscribe(resp => {
        if (!resp.Active) {
          history.push("/");
        } else {
          joinMatch(playerName, matchId)
            .pipe(take(1))
            .subscribe(
              socket => {
                setSocket(socket);
                socketObserve(socket, "match_state").subscribe(resp =>
                  setMatchState(resp as MatchState)
                );
              },
              err => console.log(err)
            );
        }
      });
    }
  }, [matchId, history, playerName]);

  const startMatch = () => {
    if (socket instanceof WebSocket) {
      socket.send("match:start");
    }
  };

  const newMatch = () => {
    if (socket instanceof WebSocket) {
      socket.send("match:new")
    }
  };

  const rotToRadians = (rotation: number) => {
    return (Math.PI * rotation) / 180;
  };

  // const currentPlayer = (): Player | null => {
  //   if (matchState && playerName) return matchState.players[playerName];
  //   return null;
  // };

  const currentMatchState = replay ? frames[replayFrame] : matchState;

  const isOwner = () => {
    if (currentMatchState && playerName) {
      return currentMatchState.Owner === playerName;
    }
    return false;
  };

  const draw = (g: PIXI.Graphics) => {
    drawWalls(g);
  };

  const drawWalls = (g: PIXI.Graphics) => {
    g.clear();

    if (currentMatchState) {
      currentMatchState.Walls.forEach(wall => {
        g.lineStyle(2, 0xffffff, 1);
        g.moveTo(
          wall.Left * currentMatchState.Grid,
          wall.Top * currentMatchState.Grid
        );
        g.lineTo(
          wall.Right * currentMatchState.Grid,
          wall.Top * currentMatchState.Grid
        );
        g.lineTo(
          wall.Right * currentMatchState.Grid,
          wall.Bottom * currentMatchState.Grid
        );
        g.lineTo(
          wall.Left * currentMatchState.Grid,
          wall.Bottom * currentMatchState.Grid
        );
        g.lineTo(
          wall.Left * currentMatchState.Grid,
          wall.Top * currentMatchState.Grid
        );
      });
    }
  };

  const allPlayers = () => {
    if (currentMatchState) {
      return Object.keys(currentMatchState.Players)
        .filter(player => !currentMatchState.Players[player].Dead)
        .map(player => (
          <Container
            x={currentMatchState.Players[player].PositionX}
            y={currentMatchState.Players[player].PositionY}
            key={player}
          >
            <Text
              text={player}
              anchor={0.5}
              y={-32}
              style={
                new PIXI.TextStyle({
                  fontFamily:
                    player === playerName
                      ? "Arial"
                      : "Arial",
                  align: "center",
                  fill: player === playerName ? "#ffffff" : "#828282",
                  fontSize: player === playerName ? "1.3rem" : "1rem",
                  padding: 10
                })
              }
            />
            <Sprite
              image={Tank}
              height={currentMatchState.Players[player].Size}
              width={currentMatchState.Players[player].Size}
              pivot={[16, 16]}
              rotation={rotToRadians(currentMatchState.Players[player].Rot)}
            />
            <Container
              x={0}
              y={32}
            >
              {[...Array(3 - currentMatchState.Players[player].BulletCount)].map((_, i) => (
                <Sprite
                  key={i}
                  image={Bullet}
                  x={-10 + 10 * i}
                  height={5}
                  width={5}
                  y={0}
                  anchor={0.5}
                />
              ))}
            </Container>
          </Container>
        ));
    }
  };

  const allBullets = () => {
    if (currentMatchState) {
      return currentMatchState.Bullets.map((bullet, i) => (
        <Sprite
          image={Bullet}
          x={bullet.PositionX}
          height={5}
          width={5}
          anchor={0.5}
          y={bullet.PositionY}
          key={i}
        />
      ));
    }
  };

  const getPlayerClassName = (player: Player) => `
    ${player.Dead ? "dead" : ""}
    ${player.Name === playerName ? "self" : ""}
    ${currentMatchState && player.Name === currentMatchState.Owner ? "owner" : ""}
  `;

  const playerArray = (currentMatch: MatchState) => {
    const all = Object.keys(currentMatch.Players).map(
      name => currentMatch.Players[name]
    );
    return [...all.filter(p => !p.Dead), ...all.filter(p => p.Dead)];
  };

  const joinFormSubmit = (e: FormEvent<HTMLFormElement>) => {
    onName(newPlayerName);
    e.preventDefault();
  };

  return (
    <div className="match-container">
      <div className="header">tankTank</div>
      <div className="game-wrapper">
        {isConnected() && currentMatchState && matchState ? (
          <>
            <div className="otherStuff">
              <OtherMatchThings
                match={matchState} // use regular match state here
                isOwner={isOwner()}
                onStart={startMatch}
                onNewGame={newMatch}
              />
            </div>
            {currentMatchState.ScoreScreen ? (
                <ScoreScreen matchState={currentMatchState}/>
            ) : (
              <>
                <div className="game">
                  <Stage
                      width={1000}
                      height={1000}
                      options={{ backgroundColor: 0x1d2230, resolution: 2 }}
                  >
                    {allPlayers()}
                    {allBullets()}
                    {currentMatchState.Powerups.map((p, i) => <PowerupSprite key={i} powerup={p}/>)}
                    <Graphics draw={draw} />
                  </Stage>
                </div>
                <div className="otherStuff">
                  <div className="players">
                    {playerArray(currentMatchState).map(p => (
                        <div key={p.Name} className={getPlayerClassName(p)}>
                          {p.Name}
                        </div>
                    ))}
                  </div>
                </div>
              </>
            )}
          </>
        ) : (
          <div className="join">
            <h1>Join match...</h1>
            <form onSubmit={joinFormSubmit}>
              <input type="text" placeholder="Name" maxLength={15} value={newPlayerName} onChange={(e) => setNewPlayerName(e.currentTarget.value)}/>
              <button type="submit">Join</button>
            </form>
          </div>
        )}
      </div>
    </div>
  );
};

export default Match;
