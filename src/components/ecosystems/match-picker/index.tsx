import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { take } from 'rxjs/operators';
import * as rest from '../../../api/rest';
import Arrows from '../../../assets/arrows';
import './matchPicker.scss';

interface IMatchPickerProps {
  onName: (name: string) => void
}

const MatchPicker: React.FC<IMatchPickerProps> = ({ onName }) => {
  const [playerName, setPlayerName] = useState('');
  const [selectedMatch, setSelectedMatch] = useState('');
  const [matches, setMatches] = useState<
    { Name: string; PlayerCount: number; Id: string }[]
  >([]);
  const history = useHistory();

  React.useEffect(() => {
    rest
      .allMatches()
      .pipe(take(1))
      .subscribe(res => setMatches(res.Games));
  }, []);

  const createMatch = () => {
    if (playerName.length > 0) {
      onName(playerName);
      rest
        .createMatch(playerName)
        .pipe(take(1))
        .subscribe(res => history.push(`/match/${res.Id}`));
    }
  };

  const joinMatch = (matchId: string) => {
    onName(playerName);
    history.push(`/match/${matchId}`);
  };

  const matchTable = (
    <div className="table-wrapper">
      <table className="match-table">
        <thead>
          <tr>
            <th>Name</th>
            <th>Players</th>
            <th />
          </tr>
        </thead>
        <tbody>
          {matches.map(match => (
            <tr key={`row-${match.Id}`}>
              <td>{match.Name}</td>
              <td>{match.PlayerCount}</td>
              <td key={`button-${match.Id}`}>
                <div
                  className={`match-select-input ${
                    match.Id === selectedMatch ? 'selected-row' : ''
                  }`}
                >
                  <input
                    maxLength={15}
                    value={playerName}
                    onChange={e => setPlayerName(e.target.value)}
                  />
                  <Arrows
                    action={() => setSelectedMatch(match.Id)}
                    active={true}
                  />
                  <button
                    className="join-button"
                    onClick={() => joinMatch(match.Id)}
                  >
                    Join!
                  </button>
                </div>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );

  const nameInput = (
    <div className="input-wrapper">
      <input
        className="name-input"
        type="text"
        value={playerName}
        maxLength={15}
        onChange={e => setPlayerName(e.target.value)}
        placeholder="Enter Name"
      />
      <Arrows action={() => createMatch()} active={playerName.length > 0} />
    </div>
  );

  return (
    <div className="landing-page">
      <div className="bottom-half">
        <div className="match-choice">
          <div className="title">Create</div>
          {nameInput}
        </div>
        <div className="match-choice">
          <div className="title">Join</div>
          {matchTable}
        </div>
      </div>
      <div className="top-half">
        <p className="title">tankTank</p>
      </div>
    </div>
  );
};

export default MatchPicker;
