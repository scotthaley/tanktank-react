import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import "./App.css";
import MatchPicker from "./components/ecosystems/match-picker";
import Match from "./components/ecosystems/match";
const App: React.FC = () => {
  const [name, setName] = React.useState('');
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route path="/match/:matchId">
            <Match playerName={name} onName={setName}/>
          </Route>
          <Route path="/">
            <MatchPicker onName={setName}/>
          </Route>
        </Switch>
      </Router>
    </div>
  );
};

export default App;
