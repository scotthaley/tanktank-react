import { of } from 'rxjs';
import { fromFetch } from 'rxjs/fetch';
import { catchError, switchMap } from 'rxjs/operators';

const apiEndpoint = () => {
  return `${process.env.REACT_APP_API_ENDPOINT || 'http://localhost:5000'}/api`;
};

export const post = (route: string, body: object = {}) => {
  return fromFetch(apiEndpoint() + route, {
    method: 'POST',
    body: JSON.stringify(body),
    headers: { 'Content-Type': 'application/json' }
  }).pipe(
    switchMap(response => {
      if (response.ok) {
        return response.json();
      } else {
        return of({ error: true, message: `Error ${response.status}` });
      }
    }),
    catchError(err => {
      console.error(err);
      return of({ error: true, message: err.message });
    })
  );
};

export const createMatch = (PlayerName: string) => {
  return post('/new', { PlayerName });
};

export const matchStatus = (GameId: string, Name: string) => {
  return post('/status', { GameId, Name });
};

export const allMatches = () => {
  return post('/games', {});
};
