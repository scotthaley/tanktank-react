export interface MatchState {
  Height: number;
  Width: number;
  Grid: number;
  Players: { [name: string]: Player };
  Powerups: Powerup[];
  Walls: Wall[];
  Bullets: Bullet[];
  Started: boolean;
  Owner: string;
  ScoreScreen: boolean;
}

export interface Bullet {
  PlayerName: string;
  PositionX: number;
  PositionY: number;
  Rot: number;
}

export interface Player {
  Name: string;
  PositionX: number;
  PositionY: number;
  Rot: number;
  Dead: boolean;
  BulletCount: number;
  Kills: number;
  Size: number;
}

export interface Wall {
  Top: number;
  Bottom: number;
  Left: number;
  Right: number;
}

export interface Powerup {
  PositionX: number;
  PositionY: number;
  Type: string;
}
