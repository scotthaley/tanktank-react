import {Observable, Subscriber} from 'rxjs';

export enum InputType {
  Forward = 'forward',
  Back = 'back',
  Left = 'left',
  Right = 'right',
  Shoot = 'shoot',
  Turn = 'turn',
  Gas = 'gas'
}

export const joinMatch = (
  name: string,
  matchId: string
): Observable<WebSocket> => {

  const socket = new WebSocket(`${process.env.REACT_APP_WS_ENDPOINT || 'ws://localhost:5000'}/join/${matchId}/${name}`);

  return new Observable((observer: Subscriber<WebSocket>) => {
    socket.addEventListener('open', () => {
      observer.next(socket);
      observer.complete();
      console.log('Joined!');
    });
    socket.addEventListener('error', (event) => {
      observer.error(event);
      observer.complete();
      console.log('Unable to join', event);
    })
  })
};

export const socketObserve = <T>(
    socket: WebSocket,
    action: string
): Observable<T> => {
  return new Observable<T>((observer: Subscriber<T>) => {
    socket.addEventListener('message', (event) => {
      if (event.data instanceof Blob) {
        const reader = new FileReader();

        reader.onload = () => {
          try {
            observer.next(JSON.parse(reader.result as any as string) as T);
          } catch {
            observer.next(reader.result as any as T);
          }
        };

        reader.readAsText(event.data);
        return;
      }
      observer.next(event.data);
    });
  })
};
