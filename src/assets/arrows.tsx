import React from "react";

type ArrowTypes = {
  action: () => void;
  active: boolean;
};

const Arrows = (props: ArrowTypes) => {
  return (
    <button
      onClick={props.action}
      style={{
        display: "flex",
        alignItems: "center",
        background: "none",
        border: "none",
        opacity: `${props.active ? "1" : 0.5}`
      }}
    >
      <svg
        aria-hidden="true"
        focusable="false"
        data-prefix="fas"
        role="img"
        className="arrows-icon"
        viewBox="0 0 448 512"
      >
        <path
          fill="currentColor"
          d="M224.3 273l-136 136c-9.4 9.4-24.6 9.4-33.9 0l-22.6-22.6c-9.4-9.4-9.4-24.6 0-33.9l96.4-96.4-96.4-96.4c-9.4-9.4-9.4-24.6 0-33.9L54.3 103c9.4-9.4 24.6-9.4 33.9 0l136 136c9.5 9.4 9.5 24.6.1 34zm192-34l-136-136c-9.4-9.4-24.6-9.4-33.9 0l-22.6 22.6c-9.4 9.4-9.4 24.6 0 33.9l96.4 96.4-96.4 96.4c-9.4 9.4-9.4 24.6 0 33.9l22.6 22.6c9.4 9.4 24.6 9.4 33.9 0l136-136c9.4-9.2 9.4-24.4 0-33.8z"
        ></path>
      </svg>
    </button>
  );
};

export default Arrows;
